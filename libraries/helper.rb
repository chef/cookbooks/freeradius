module Freeradius
  # Helper functions for the FreeRADIUS cookbook
  module Helper
    module_function

    # Replace every occurance of ${} in a directory value
    # First parameter must be a name like "confdir"
    def freeradius_absolute(name)
      freeradius_resolve(node['freeradius']['config'][name])
    end

    # Lazy load default FreeRADIUS raddbdir path
    def freeradius_dbdir
      value = node['freeradius']['config']['raddbdir']
      return value unless value.nil?

      # same value in debian and upstream
      node.default['freeradius']['config']['raddbdir'] =
        '/etc/freeradius/3.0'
    end

    # Lazy load default FreeRADIUS group
    def freeradius_group
      value = node['freeradius']['config']['security']['group']
      return value unless value.nil?

      node.default['freeradius']['config']['security']['group'] =
        if platform_family?('debian')
          'freerad'
        else # upstream
          'radius'
        end
    end

    # Resolve a value by replacing every ${attr} in it
    def freeradius_resolve(value)
      freeradius_dbdir # initialize raddbdir
      config = node['freeradius']['config']
      loop do
        oldvalue = value.dup
        value = value.gsub(/\${[^}]+}/) { |m| config[m[2..-2]] }
        break if oldvalue.eql?(value)
      end
      value
    end

    # Lazy set default FreeRADIUS user
    def freeradius_user
      value = node['freeradius']['config']['security']['user']
      return value unless value.nil?

      node.default['freeradius']['config']['security']['user'] =
        if platform_family?('debian')
          'freerad'
        else # upstream
          'radius'
        end
    end

    def freeradius_secret(str)
      return str unless str.is_a? String
      return str unless str =~ /{vault:[^:]+:[^:]+:[^:]+}/

      require 'chef-vault'
      str.gsub(/{vault:[^:]+:[^:]+:[^:]+}/) do |secret|
        values = secret[1..-2].split(':')
        vault = ChefVault::Item.load(values[1], values[2])
        vault.nil? ? nil : vault[values[3]]
      end
    end
  end
end
