#
# Cookbook:: freeradius
# Recipe:: v3
#
# Copyright:: 2019-2020, GSI Helmholtzzentrum fuer Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if platform?('debian') && node['platform_version'].to_i < 9
  raise 'FreeRADIUS v3.X is not available on Debian < 9'
elsif platform?('ubuntu') && node['platform_version'].to_f < 18.04
  raise 'FreeRADIUS v3.X is not available on Ubuntu < 18.04'
end

Chef::Recipe.include Freeradius::Helper

# common values from node
raddbdir = freeradius_dbdir
user = freeradius_user
group = freeradius_group
keep_mod_eap = !node['freeradius']['remove_eap'] &&
               (node['freeradius']['mod_eap'] || []).empty?

# software packages {{{
%w(freeradius freeradius-utils).each do |pkg|
  package(pkg)
end

# install some mods only if needed
%w(ldap sql).each do |mod|
  remove = (node['freeradius']['mod_' + mod] || []).empty?
  package 'freeradius-' + mod do
    action :remove if remove
    ignore_failure remove
  end
end
# }}}

# configuration files {{{
template(raddbdir + '/clients.conf') do
  source 'v3/clients.conf.erb'
  owner user
  group group
  mode '0660'
  variables(node['freeradius']['clients'] || {})
  helpers(Freeradius::Helper)
  notifies :restart, 'service[freeradius]'
end

template(raddbdir + '/dictionary') do
  source 'v3/dictionary.erb'
  owner user
  group group
  mode '0660'
  variables(entries: node['freeradius']['dictionary'] || [])
  notifies :restart, 'service[freeradius]'
end

template(raddbdir + '/proxy.conf') do
  source 'v3/proxy.conf.erb'
  owner user
  group group
  mode '0660'
  variables(node['freeradius']['proxy'] || {})
  notifies :restart, 'service[freeradius]'
end

template(raddbdir + '/radiusd.conf') do
  source 'v3/radiusd.conf.erb'
  owner user
  group group
  mode '0660'
  variables(node['freeradius']['config'] || {})
  notifies :restart, 'service[freeradius]'
end
# }}}

# auxiliary files {{{
(node['freeradius']['files'] || {}).each do |name, content|
  name = if ::File.dirname(name).eql?('.')
           "#{raddbdir}/#{name}"
         else
           freeradius_resolve(name)
         end
  if (content = freeradius_secret(content)).nil?
    Chef::Log.error "Could not get secret content for #{name}"
    next
  end
  file(name) do
    content content
    owner user
    group group
    backup false
    mode '0660'
    notifies :restart, 'service[freeradius]'
  end
end
# }}}

# mod_eap {{{

# keep the link to the original EAP configuration
# only if no custom ones are defined
link(raddbdir + '/mods-enabled/eap') do
  to(raddbdir + '/mods-available/eap')
  ignore_failure true
  action :delete unless keep_mod_eap
  notifies :restart, 'service[freeradius]'
end

# create custom EAP configurations
unless (node['freeradius']['mod_eap'] || []).empty?
  node['freeradius']['mod_eap'].each do |name, attrs|
    template(raddbdir + '/mods-enabled/eap_' + name) do
      source 'v3/mod_eap.erb'
      owner user
      group group
      mode '0660'
      attrs = attrs.dup # duplicate immutable hash
      attrs['name'] = name
      attrs['tls'] ||= {}
      attrs['peap'] ||= {}
      attrs['ttls'] ||= {}
      attrs['types'] ||= %w(md5 leap gtc tls ttls peap mschapv2 fast)
      variables attrs
      notifies :restart, 'service[freeradius]'
    end
  end
end
# }}}

# mod_files {{{

# because we can not configure default files with this
# cookbook we just deactivate the upstream module
link(raddbdir + '/mods-enabled/files') do
  to(raddbdir + '/mods-available/files')
  ignore_failure true
  notifies :restart, 'service[freeradius]'
end

unless (node['freeradius']['mod_files'] || []).empty?
  node['freeradius']['mod_files'].each do |name, attrs|
    template(raddbdir + '/mods-enabled/files_' + name) do
      source 'v3/mod_files.erb'
      owner user
      group group
      mode '0660'
      attrs = attrs.dup # duplicate immutable hash
      attrs['name'] = name
      variables attrs
      notifies :restart, 'service[freeradius]'
    end
    directory(raddbdir + '/mods-config/files_' + name) do
      owner user
      group user
      mode '0755'
    end
    %w(accounting authorize pre-proxy).each do |file|
      if attrs['ignore_' + file]
        file(raddbdir + '/mods-config/files_' + name + '/' + file) do
          owner user
          group group
          action :create_if_missing
        end
      elsif attrs[file] || attrs[file.sub(/_/, '-')]
        template(raddbdir + '/mods-config/files_' + name + '/' + file) do
          source 'v3/mod_files_users.erb'
          owner user
          group group
          mode '0660'
          variables(users: attrs[file] || attrs[file.sub(/_/, '-')] || {})
          helpers(Freeradius::Helper)
          notifies :restart, 'service[freeradius]'
        end
      else
        file(raddbdir + '/mods-config/files_' + name + '/' + file) do
          content '# WARNING! This file is managed by chef ' \
                  "and any changes will be overwritten.\n"
          notifies :restart, 'service[freeradius]'
        end
      end
    end
  end
end
# }}}

# mod_ldap {{{

# because we can not configure default ldap with this
# cookbook we just deactivate the upstream module
link(raddbdir + '/mods-enabled/ldap') do
  to(raddbdir + '/mods-available/ldap')
  action :delete
  notifies :restart, 'service[freeradius]'
end

unless (node['freeradius']['mod_ldap'] || []).empty?
  node['freeradius']['mod_ldap'].each do |name, attrs|
    template(raddbdir + '/mods-enabled/ldap_' + name) do
      source 'v3/mod_ldap.erb'
      owner user
      group group
      mode '0660'
      attrs = attrs.dup # duplicate immutable Hash
      attrs['name'] = name
      attrs['client'] ||= {}
      attrs['group'] ||= {}
      attrs['log'] ||= {}
      attrs['user'] ||= {}
      variables attrs
      helpers(Freeradius::Helper)
      notifies :restart, 'service[freeradius]'
    end
  end
end
# }}}

# sites {{{
link(raddbdir + '/sites-enabled/default') do
  to(raddbdir + '/sites-available/default')
  owner user
  group group
  action :delete if node['freeradius']['remove_default_server']
  notifies :restart, 'service[freeradius]'
end

link(raddbdir + '/sites-enabled/inner-tunnel') do
  to(raddbdir + '/sites-available/inner-tunnel')
  owner user
  group group
  action :delete if node['freeradius']['remove_inner_tunnel_server']
  notifies :restart, 'service[freeradius]'
end

# remove undefined sites
defined_sites = %w(default inner-tunnel) +
                (node['freeradius']['virtual_server'] || {}).keys
Dir[raddbdir + '/sites-enabled/*'].each do |f|
  next if defined_sites.include? File.basename(f)
  file(f) do
    manage_symlink_source false
    action :delete
    notifies :restart, 'service[freeradius]'
  end
end

# collect clients per virtual server
virtual_server_clients = {}
((node['freeradius']['clients'] || {})['groups'] || {}).each do |cgroup, attrs|
  if attrs['virtual_server']
    virtual_server_clients[attrs['virtual_server']] = cgroup
  end
end

unless (node['freeradius']['virtual_server'] || []).empty?
  node['freeradius']['virtual_server'].each do |name, attrs|
    attrs = attrs.dup # duplicate immutable Hash
    eap = 'eap' + (attrs['template_eap'] ? '_' + attrs['template_eap'] : '')

    # apply template (if set)
    case attrs['template']
    when 'default'
      attrs['authorize'] ||= ['filter_username', 'preprocess', 'chap', 'mschap',
                              'digest', 'suffix', eap + ' {', ' ok = return',
                              '}', 'pap']
      attrs['authenticate'] ||= ['digest', eap]
      attrs['preacct'] ||= %w(preprocess acct_unique suffix)
      attrs['accounting'] ||= ['detail', 'unix', '-sql', 'exec',
                               'attr_filter.accounting_response']
      attrs['post_auth'] ||= [
        'if (session-state:User-Name && reply:User-Name && request:User-Name ' \
        '&& (reply:User-Name == request:User-Name)) {', ' update reply {',
        '  &User-Name !* ANY', ' }', '}', 'update {', ' &reply: += ' \
        '&session-state:', '}', '-sql', 'exec', 'remove_reply_message_if_eap',
        'Post-Auth-Type REJECT {', ' -sql', ' attr_filter.access_reject',
        ' ' + eap, ' remove_reply_message_if_eap', '}',
        'Post-Auth-Type Challenge {', '}'
      ]
      attrs['post_proxy'] ||= [eap]
    when 'inner-tunnel'
      attrs['authorize'] ||= ['filter_username', 'chap', 'mschap', 'suffix',
                              'update control {', ' &Proxy-To_Realm := LOCAL',
                              '}', eap + '{', ' ok = return', '}', 'pap']
      attrs['authenticate'] ||= [eap]
      attrs['session'] ||= %w(radutmp)
      attrs['post_auth'] ||= ['-sql', 'Post-Auth-Type REJECT {', ' -sql',
                              ' attr_filter.access_reject',
                              ' update outer.session-state {',
                              '  &Module-Failure-Message := ' \
                              '&request:Module-Failure-Message', ' }', '}']
      attrs['post_proxy'] ||= [eap]
    end

    template(raddbdir + '/sites-enabled/' + name.downcase) do
      source 'v3/virtual_server.erb'
      owner user
      group group
      mode '0660'
      attrs['name'] = name
      attrs['listen'] ||= []
      attrs['listen'] = [attrs['listen']] unless attrs['listen'].is_a?(Array)
      attrs['authorize_modules'] ||= %w(expiration logintime)
      attrs['preacct_modules'] ||= []
      # add files to preacct modules if available and needed
      if (node['freeradius']['mod_files'] || []).empty? &&
         attrs['preaacct_modules'].include?('files')
        attrs['preaacct_modules'].add 'files'
      end
      variables attrs
      notifies :restart, 'service[freeradius]'
    end
  end
end
# }}}

# service environment {{{
template('/etc/default/freeradius') do
  source 'v3/environment.erb'
  owner 'root'
  group 'root'
  mode '0664'
  variables(options: node['freeradius']['environment'] || '')
  notifies :restart, 'service[freeradius]'
end

service 'freeradius' do
  action %i(enable start)
end
# }}}
