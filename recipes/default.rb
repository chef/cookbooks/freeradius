# frozen-string-literal: true

# Cookbook:: freeradius
# Recipe:: default
#
# Install and configure the appropriate version of FreeRADIUS
#
# Copyright:: 2019-2020, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

case node['platform']
when 'debian'
  raise "Version #{node['platform_version']} of Debian is not supported." \
    if node['platform_version'].to_f < 9.0

  include_recipe 'freeradius::v3'

when 'ubuntu'
  raise "Version #{node['platform_version']} of Ubuntu is not supported." \
    if node['platform_version'].to_f < 18.04

  include_recipe 'freeradius::v3'

else
  raise "The platform #{node['platform']} is not configured. " \
    'If you know what you need, select a recipe other then default.'
end
