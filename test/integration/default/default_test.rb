# InSpec test for recipe freeradius::default
#
# Many of the tests fail, because FreeRADIUS does not seem to differentiate
# between clients and ports when the requests come from `radtest`.

eap_client = '192.168.0.20'
eap_ports = [1812, 18_120]
mac_client = '192.168.0.30'
mac_ports = [1812, 18_121]
secret = 'testing123'

describe package('freeradius') do
  it { should be_installed }
end

describe systemd_service('freeradius.service') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

# let freeradius check the configuration
describe command('/usr/sbin/freeradius -C') do
  its(:exit_status) { should eq 0 }
end

# test mac authentication
mac_ports.each do |port|
  describe command("radtest 01-23-45-67-89-ab irrelevant 127.0.0.1:#{port} " \
                   "10 #{secret} 0 #{mac_client}") do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should match(/Received Access-Accept/) }
  end
  describe command("radtest 01-23-45-67-89-ac irrelevant 127.0.0.1:#{port} " \
                   "10 #{secret} 0 #{mac_client}") do
    its(:exit_status) { should_not eq 0 }
    its(:stdout) { should match(/Received Access-Reject/) }
  end
end
# requests to :1812 should be answered by wifi
describe command('radtest 01-23-45-67-89-ab irrelevant 127.0.0.1:1812 ' \
                 "10 #{secret} 0 #{eap_client}") do
  its(:exit_status) { should_not eq 0 }
  its(:stdout) { should match(/Received Access-Reject/) }
end

# non-EAP requests
eap_ports.each do |port|
  %w(pap chap mschap).each do |auth| # eap-md5?
    # local request for existing user
    describe command("radtest -t #{auth} admin admintest123 " \
                     "127.0.0.1:#{port} 10 testing123 0") do
      # non eap requests should only work in "inner tunnels"
      if port == 1812
        its(:exit_status) { should_not eq 0 }
        its(:stdout) { should match(/Received Access-Reject/) }
      else
        its(:exit_status) { should eq 0 }
        its(:stdout) { should match(/Received Access-Accept/) }
      end
    end

    # local request for existing user with wrong password
    describe command("radtest -t #{auth} admin wrongpass " \
                     "127.0.0.1:#{port} 10 testing123 0") do
      its(:exit_status) { should_not eq 0 }
      its(:stdout) { should match(/Received Access-Reject/) }
    end
  end
end

# local EAP request for existing user
describe command('cat <<REQ | radeapclient -x 127.0.0.1 auth testing123
User-Name = "admin"
EAP-MD5-Password = "admintest123"
NAS-IP-Address = 127.0.0.1
EAP-Code = Response
EAP-Id = 210
EAP-Type-Identity = "anonymous@example.com"
Message-Authenticator = 0x00
NAS-Port = 0
REQ') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/Received Access-Accept/) }
end

describe command('cat <<REQ | radeapclient -x 127.0.0.1 auth testing123
User-Name = "admin@example.com"
EAP-MD5-Password = "admintest123"
NAS-IP-Address = 127.0.0.1
EAP-Code = Response
EAP-Id = 210
EAP-Type-Identity = "anonymous@example.com"
Message-Authenticator = 0x00
NAS-Port = 0
REQ') do
  its(:exit_status) { should eq 0 }
  its(:stdout) { should match(/Received Access-Accept/) }
end
