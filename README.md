# FreeRADIUS

This cookbook installs FreeRADIUS on a machine. All attributes are mostly derived directly from the FreeRADIUS configuration files. Therefor it should be possible to configure everything which FreeRADIUS has to offer.

## Attributes

Attributes reside under `node['freeradius']`. Without setting any custom values FreeRADIUS will be installed and no OS specific settings will be changed.

The attribute structure imitates the structure in FreeRADIUS config files, for example `['freeradius']['config']['raddbdir']` and `['freeradius']['config']['security']['user']`.

Additional attributes are:

 * `['freeradius']['config']['listen']` - Array of listen sections, each with attributes `ipaddr`, `ipv6addr`, `port`, `interface`, `clients`, `recv_buff`, `limit`, `proto`, `virtual_server`, `tls.private_key_password`, `tls.private_key_file`, `tls.certificate_file`, `tls.ca_file`, `tls.dh_file`, `tls.require_client_cert`, `tls.check_cert_issuer`, `check_cert_cn` and `tls.fragment_size`.
 * `['freeradius']['remove_eap']` - Deactivate the default eap module even if you have not defined your own
 * `['freeradius']['remove_default_server']` - Deactivate the virtual server `default`
 * `['freeradius']['remove_inner_tunnel_server']` - Deactivate the virtual server `inner_tunnel`
 * for others see below

### Clients, Groups

Clients and groups are defined under `['freeradius']['clients']`. Set `['freeradius']['clients']['localhost']` to `true` to allow localhost as a client. Other clients are defined as follows:

```ruby
['freeradius']['clients'] = {
  'groups' = {
    '<name>' => {
      'proto' => 1812, # default client attributes ...
      'nosection' => false, # use group only for default values
      'clients' => [
        '<name>' => {
          # any of ipaddr, secret, proto , require_message_authenticator, virtual_server
        },
        # alternative format using only default values
        '<name>' => '<ipaddr>'
      ]
    }
  },
  'clients' = [ # just like 'clients' in 'groups' section
}
```

### Dictionary

The dictionary can be declared as an array of lines (strings) under `['freeradius']['dictionary']`.

### Proxy (home server, home server pool, realm)

```ruby
['freeradius']['proxy'] = {
  'home_servers' => {
    '<name>' => {
    # any of ipaddr, port, secret, proto, require_message_authenticator, status_check, virtual_server
    'tls' => {
    # any of private_key_password, private_key_file, certificate_file, ca_file, dh_file, require_client_cert, check_cert_issuer, check_cert_cn, fragment_size
    },
    # alternative format using only default values
    '<name>' => '<ipaddr>'
  },
  'home_server_pools' => {
    '<name>' => {
      # type and home_server
    }
  },
  'realms' => {
    '<name>' => {
      # any of type, auth_pool, virtual_server and nostrip
    }
  }
}
```

### Mod_Files

```ruby
['freeradius']['mod_files'] = {
  '<name>' => { # real name will be files_<name>
    'key' => nil, # optional
    'ignore_<file>' => true, # do not manage files for accounting, authorize or pre-proxy
    '<accounting|authorize|pre-proxy>' => {
      '<username>' => [], # Request attributes to authenticate the user
      '<username>' => { # alternative format with reply items
        'check_items' => [],
        'reply_items' => []
      }
    }
  }
}
```

### Mod_LDAP

```ruby
['freeradius']['mod_ldap'] = {
  '<name> => { # real name will be ldap_<name>
    # any of server, port, identity, password, base_dn, edir, edir_autz, user_dn
    'user' => {
      # any of base_dn, filter, access_attribute
    },
    'group' => {
      # any of base_dn, filter and scope
    },
    'client' => {
      # any of base_dn and filter
    },
    'update' = {
      '<radius_item>' => '<ldap_item>'
    }
  }
}
```

### Sites / Virtual servers

Because virtual servers can be very complex, the available attributes are mostly arrays which hold lines for the corresponding sections. There are a few support elements like templates and automatic inflation of the authenticate section. Spaces at the begining of a line are translated to tabulators. So section contents can be written readable and short.

```ruby
['freeradius']['virtual_server'] = {
  '<name>' => {
    'listen' => [], # listen sections as in ['freeradius']['config']['listen']
    'clients' => '<name>', # alternatively a hash as in ['freeradius']['clients']
    'template' => '<default|inner-tunnel>', # use default values from a template
    'template_eap' => '<name>', # EAP module to use in templates
    'authorize' => [], # seperate lines for the authorize section
    'authenticate' => [], # seperate lines for the authenticate section
    # Auth-Type PAP, CHAP and MS-CHAP will automatically be inserted if used in 'authorize'
    '<preacct|session|post_auth|pre_proxy|post_proxy>' => [] # seperate lines
  }
}
```

## Files

Additional files can be produced with `node['freeradius']['files']`. The attribute must be a hash containing filenames and contents. If a filename has no directory it will be created under `raddbdir`. Filenames can contain FreeRADIUS variables like `${certdir}`. Contents can contain secrets (see below).

## Recipes

 * `default` - Select the configuration version by the nodes platform. If the platform or version is not supported this will raise an exception.
 * `v3` - Install and configure version 3.x of FreeRADIUS.

## Secrets

Some strings can contain requests for Chef Vault values. To do this they contain substrings like `{vault:"vault":"item":"value"}`. `"vault"`, `"item"` and `"value"` form the path to the value.

Strings allowing secrets are:

 * `secret` attributes under `node['freeradius']['clients']`
 * Check attributes under `node['freeradius']['mod_files'][xxx]['user']['authorize']`
 * `node['freeradius']['mod_ldap'][xxx]['password']`
 * The contents of `node['freeradius']['files']`

## License

Copyright 2019-2020, GSI Helmholtzzentrum für Schwerionenforschung

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
