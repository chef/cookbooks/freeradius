default['freeradius']['environment'] = ''

default['freeradius']['config']['prefix'] = '/usr'
default['freeradius']['config']['exec_prefix'] = '/usr'
default['freeradius']['config']['sysconfdir'] = '/etc'
default['freeradius']['config']['localstatedir'] = '/var'
default['freeradius']['config']['sbindir'] = '${exec_prefix}/sbin'
default['freeradius']['config']['logdir'] = '/var/log/freeradius'
default['freeradius']['config']['raddbdir'] = nil # defined by platform
default['freeradius']['config']['radacctdir'] = '${logdir}/radacct'
default['freeradius']['config']['name'] = 'freeradius' # radiusd?
default['freeradius']['config']['confdir'] = '${raddbdir}'
default['freeradius']['config']['modconfdir'] = '${confdir}/mods-config'
default['freeradius']['config']['certdir'] = '${confdir}/certs'
default['freeradius']['config']['cadir'] = '${confdir}/certs'
default['freeradius']['config']['run_dir'] = '${localstatedir}/run/${name}'
default['freeradius']['config']['db_dir'] = '${raddbdir}'
default['freeradius']['config']['libdir'] = '/usr/lib/freeradius'
default['freeradius']['config']['pidfile'] = '${run_dir}/${name}.pid'
default['freeradius']['config']['correct_escapes'] = true
default['freeradius']['config']['max_request_time'] = 30
default['freeradius']['config']['cleanup_delay'] = 5
default['freeradius']['config']['max_requests'] = 16_384
default['freeradius']['config']['hostname_lookups'] = false

default['freeradius']['config']['log']['destination'] = 'files'
default['freeradius']['config']['log']['colourise'] = true
default['freeradius']['config']['log']['file'] = '${logdir}/radius.log'
default['freeradius']['config']['log']['syslog_facility'] = 'daemon'
default['freeradius']['config']['log']['stipped_names'] = false
default['freeradius']['config']['log']['auth'] = false
default['freeradius']['config']['log']['auth_accept'] = nil
default['freeradius']['config']['log']['auth_reject'] = nil
default['freeradius']['config']['log']['auth_badpass'] = false
default['freeradius']['config']['log']['auth_goodpass'] = false
default['freeradius']['config']['log']['msg_badpass'] = nil
default['freeradius']['config']['log']['msg_goodpass'] = nil
default['freeradius']['config']['log']['msg_denied'] =
  'You are already logged in - access denied'

default['freeradius']['config']['security']['chroot'] = nil
default['freeradius']['config']['security']['user'] = nil # defined by platform
default['freeradius']['config']['security']['group'] = nil # defined by platform
default['freeradius']['config']['security']['allow_core_dumps'] = false
default['freeradius']['config']['security']['max_attributes'] = 200
default['freeradius']['config']['security']['reject_delay'] = 1
default['freeradius']['config']['security']['status_server'] = true
default['freeradius']['config']['security']['allow_vulnerable_openssl'] = false

default['freeradius']['config']['thread_pool']['start_servers'] = 5
default['freeradius']['config']['thread_pool']['max_servers'] = 32
default['freeradius']['config']['thread_pool']['min_spare_servers'] = 3
default['freeradius']['config']['thread_pool']['max_spare_servers'] = 10
default['freeradius']['config']['thread_pool']['max_requests_per_server'] = 0
default['freeradius']['config']['thread_pool']['auto_limit_acct'] = false

default['freeradius']['config']['listen'] = []

default['freeradius']['clients']['localhost'] = {}
default['freeradius']['clients']['groups'] = {}
default['freeradius']['clients']['clients'] = {}

default['freeradius']['dictionary'] = []
default['freeradius']['files'] = {}
default['freeradius']['proxy'] = {}
default['freeradius']['remove_eap'] = false
default['freeradius']['remove_default_server'] = false
default['freeradius']['remove_inner_tunnel_server'] = false
default['freeradius']['mod_files'] = {}
default['freeradius']['mod_ldap'] = {}
default['freeradius']['virtual_server'] = {}
